import React from "react";
import "./login.css";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  // onChange = (e) => {
  //   let targetname = [e.target.name];
  //   this.setState({ [e.target.name]: e.target.value });
  //   if (targetname[0] === "username") {
  //     this.setState({ username: "" });
  //   } else if (targetname[0] === "email") {
  //     this.setState({ email: "" });
  //   } else if (targetname[0] === "password") {
  //     this.setState({ password: "" });
  //   } else {
  //     // some other input, do nothing
  //   }
  // };

  formValidate = () => {
    const { username, email, password } = this.state;
    let isValid = true;
    const errors = {};
    if (username.trim().length < 6 || username.trim().length > 9) {
      errors.usernameErr = "Username must be of length 6 or higher";
      isValid = false;
    }
    if (!username) {
      errors.usernameErr = "*Please enter your username.";
      isValid = false;
    }
    //"^(?=[a-zA-Z]{6,9}$)(?!.[.]{2})[^.].[^_.]$"
    if (typeof username !== "undefined") {
      if (!username.match(/^([a-zA-Z]*$)/)) {
        errors.usernameErr = "Please enter alphabet characters only.";
        isValid = false;
      }
    }

    if (!email) {
      errors.email = "*Please enter your email.";
      isValid = false;
    }
    if (typeof email !== "undefined") {
      if (!email.match(/^[a-z0-9]+@[a-z0-9]+?\.[a-z]{2,3}$/)) {
        isValid = false;
        errors.email = "Please enter valid email.";
      }
    }

    if (typeof password !== "undefined") {
      if (
        !password.match(
          /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/
        )
      ) {
        isValid = false;
        errors.password = "Please enter valid password.";
      }
    }
    this.setState({ errors });
    return isValid;
  };

  onSubmit = (e) => {
    e.preventDefault();
    const isValid = this.formValidate();
    if (isValid === true) {
      //send username and password to server
      console.log(this.state);
      this.setState({ username: "", email: "", password: "" });
      alert("Form submitted successfully");
    }
  };
  render() {
    const { username, password, email, errors } = this.state;
    return (
      <>
        <div className="container">
          <h1>Please Login</h1>
          <form onSubmit={this.onSubmit} autoComplete="off">
            <div className="form-control">
              <input
                type="text"
                name={"username"}
                placeholder="User name"
                value={username}
                required=""
                onChange={this.onChange}
              />
              <div className="errorMsg">{this.state.errors.usernameErr}</div>
            </div>
            <div className="form-control">
              <input
                type="text"
                name={"email"}
                placeholder="Email"
                value={email}
                required=""
                onChange={this.onChange}
                autoComplete="off"
              />
              <div className="errorMsg">{this.state.errors.email}</div>
            </div>

            <div className="form-control">
              <input
                type="password"
                name={"password"}
                placeholder="Password"
                value={password}
                required=""
                onChange={this.onChange}
              />
              <div className="errorMsg">{this.state.errors.password}</div>
            </div>
            <button className="btn">Login</button>
            <p className="text">
              Don't have an account? <a href="#">Register</a>
            </p>
          </form>
        </div>
      </>
    );
  }
}

export default Login;
